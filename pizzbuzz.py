def print_num():
	for i in range(1, 16):
		if i % 15 == 0 :
			print('fizzbuzz')
			continue
		if i % 5 == 0 :
			print('buzz')
			continue
		if i % 3 == 0 : 
			print('fizz')
		else:
			print(i)


def print_pizz():
	for i in range(1, 16) :
		if i % 5 == 0 or i % 3 == 0:
			print('fizz' * (i % 3 == 0) + 'buzz' * (i % 5 == 0))
		else :
			print(i)


if __name__ == '__main__':
	print_pizz()
